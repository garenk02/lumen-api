Teacher:
	#id
	- name
	- address
	- phone
	- profession
	__________________________________________________________
	=> A teacher teaches many courses
--------------------------------------------------------------

Student:
	#id
	- name
	- address
	- phone
	- career
	__________________________________________________________
	=> A student takes many courses
--------------------------------------------------------------

Course:
	#id
	- title
	- description
	- value
	_________________________________________________________
	=> A course is teached by one teacher (one to many - 1:N)
	=> A course is taken by many students (many to many - N:N)
--------------------------------------------------------------