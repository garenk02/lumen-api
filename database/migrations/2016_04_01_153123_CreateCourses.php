<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('short_name');
            $table->string('full_name');
            $table->string('descrription');
            $table->date('start_date');
            //0=Not Publish //1=publish
            $table->char('status')->default('0');
            $table->text('tag');
            $table->string('requirement');
            $table->string('audiance_target');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
