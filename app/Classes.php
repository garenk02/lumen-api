<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $fillable=['name','grade','school_id','max_capacity'];

    public function school()
    {
    	return $this->belongsTo('App\School');
    }

}

