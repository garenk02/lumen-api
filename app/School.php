<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable=['name','address'];
    
    public function classes()
    {
    	return $this->hasMany('App\Classes');
    }

    public function village()
    {
    	return $this->hasOne('App\Village');
    }
}
