<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;

class TeacherController extends Controller
{
	public function __construct()
	{
		$this->middleware('oauth2',['except' => ['index', 'show']]);
	}

	public function index()
	{
		$teacher = Teacher::all();
		return $this->createSuccessResponse($teacher, 200);
	}

	public function show($id)
	{
		$teacher = Teacher::find($id);

		if($teacher) {
			return $this->createSuccessResponse($teacher, 200);
		}

		return $this->createErrorResponse("The data does not exist", 404);
	}

	public function store(Request $request)
	{
		$this->validateRequest($request);

		$teacher = Teacher::create($request->all());

		return $this->createSuccessResponse("Data successfully created", 201);
	}

	public function update(Request $request, $teacher_id)
	{
		$teacher = Teacher::find($teacher_id);

		if($teacher) {
			$this->validateRequest($request);

			$teacher->name = $request->get('name');
			$teacher->address = $request->get('address');
			$teacher->phone = $request->get('phone');
			$teacher->profession = $request->get('profession');

			$teacher->save();

			return $this->createSuccessResponse("Data successfully updated", 200);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}

	public function destroy()
	{
		$teacher = Teacher::find($teacher_id);

		if($teacher) {
			$courses = $teacher->courses();

			if(sizeof($courses) > 0) {
				return $this->createErrorResponse("Sorry, data cannot be deleted", 409);
			}

			$teacher->delete();

			return $this->createSuccessResponse("Data successfully deleted", 200);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}

	private function validateRequest($request)
	{
		$rules =
		[
			'name' => 'required',
			'address' => 'required',
			'phone' => 'required|numeric',
			'profession' => 'required|in:engineering,math,physics',
		];

		$this->validate($request, $rules);
	}
}