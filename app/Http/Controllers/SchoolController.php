<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;

class SchoolController extends Controller
{
	public function __construct()
	{
		// $this->middleware('oauth2',['except' => ['index', 'show']]);
	}

	public function index()
	{
		$school = School::all();
		return $this->createSuccessResponse($school, 200);
	}

	public function show($id)
	{
		$school = School::find($id);

		if($school) {
			return $this->createSuccessResponse($school, 200);
		}

		return $this->createErrorResponse("The data does not exist", 404);
	}

	public function store(Request $request)
	{
		$this->validateRequest($request);

		$school = School::create($request->all());

		return $this->createSuccessResponse("Data successfully created", 201);
	}

	public function update(Request $request, $school_id)
	{
		$school = School::find($school_id);

		if($school) {
			$this->validateRequest($request);

			$school->name = $request->get('name');
			$school->address = $request->get('address');
			$school_id->village_id = $request->get('village_id');
			$school->zip_code = $request->get('zip_code');
			$school->zip_code = $request->get('school_status');

			$school->save();

			return $this->createSuccessResponse("Data successfully updated", 200);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}

	public function destroy($school_id)
	{
		$school = School::find($school_id);

		if($school) {
			$school->delete();

			return $this->createSuccessResponse("Data successfully deleted", 200);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}

	private function validateRequest($request)
	{
		$rules =
		[
			'name' => 'required',
			'address' => 'required',
			'village_id' => 'required|numeric',
			'school_status' => 'required|in:1,2',
		];

		$this->validate($request, $rules);
	}
}