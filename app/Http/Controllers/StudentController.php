<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentController extends Controller
{
	public function __construct()
	{
		$this->middleware('oauth2',['except' => ['index', 'show']]);
	}

	public function index()
	{
		$student = Student::all();
		return $this->createSuccessResponse($student, 200);
	}

	public function show($id)
	{
		$student = Student::find($id);

		if($student) {
			return $this->createSuccessResponse($student, 200);
		}

		return $this->createErrorResponse("The data does not exist", 404);
	}

	public function store(Request $request)
	{
		$this->validateRequest($request);

		$student = Student::create($request->all());

		return $this->createSuccessResponse("Data successfully created", 201);
	}

	public function update(Request $request, $student_id)
	{
		$student = Student::find($student_id);

		if($student) {
			$this->validateRequest($request);

			$student->name = $request->get('name');
			$student->address = $request->get('address');
			$student->phone = $request->get('phone');
			$student->career = $request->get('career');

			$student->save();

			return $this->createSuccessResponse("Data successfully updated", 200);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}

	public function destroy($student_id)
	{
		$student = Student::find($student_id);

		if($student) {
			$student->courses()->detach();
			$student->delete();

			return $this->createSuccessResponse("Data successfully deleted", 200);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}

	private function validateRequest($request)
	{
		$rules =
		[
			'name' => 'required',
			'address' => 'required',
			'phone' => 'required|numeric',
			'career' => 'required|in:engineering,math,physics',
		];

		$this->validate($request, $rules);
	}
}