<?php namespace App\Http\Controllers;

use App\Course;
use App\Student;

class CourseStudentController extends Controller
{
	public function __construct()
	{
		$this->middleware('oauth2',['except' => ['index']]);
	}

	public function index($course_id)
	{
		$course = Course::find($course_id);

		if($course) {
			$students = $course->students;

			return $this->createSuccessResponse($students, 200);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}

	public function store($course_id, $student_id)
	{
		$course = Course::find($course_id);

		if($course) {
			$student = Student::find($student_id);

			if($student) {
				if($course->students()->find($student_id)) {
					return $this->createErrorResponse("Data already exist", 409);
				}
				$course->students()->attach($student_id);

				return $this->createSuccessResponse("Data successfully added", 201);
			}

			return $this->createErrorResponse("Data does not exist", 404);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}

	public function destroy($course_id, $student_id)
	{
		$course = Course::find($course_id);

		if($course) {
			$student = Student::find($student_id);

			if($student) {
				if(!$course->students()->find($student_id)) {
					return $this->createErrorResponse("Data does not exist", 404);
				}
				$course->students()->detach($student_id);

				return $this->createSuccessResponse("Data successfully removed", 200);
			}

			return $this->createErrorResponse("Data does not exist", 404);
		}

		return $this->createErrorResponse("Data does not exist", 404);
	}
}