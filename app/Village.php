<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $fillable=['name','distric_id'];
    public function district()
    {
    	return $this->belongsTo('App\District');
    }

    public function school()
    {
    	return $this->belongsTo('App\School');
    }

}
